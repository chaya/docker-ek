##Forked from 
    https://github.com/blacktop/docker-elastic-stack

##Changes from original images
  * Added geo ip plugin
  * Added user agent plugin
  * Added basic authentication for http access
  * Removed Logstash

##Change kibana's & elastic's nginx default user and password
    $ docker run -d --name elstack \
      -p 80:80 \        #Protected kibana access
      -p 443:443 \      #Protected ssl kibana access
      -p 5601:5601 \    #Unprotected kibana access
      -p 9200:9200 \    #Unprotected elastic access
      -p 9201:9201 \    #Protected elastic access
      -e SSL=true \     #Enable SSL or not
      -e ELSK_USER="admin" \
      -e ELSK_PASS="S3cr3T" \
      chaya/ek

### To supply your own SSL certs  

Create certs with the following names

 - `kibana.key`  
 - `kibana.crt`  

```bash
$ docker run -d --name elstack \
  -p 80:80 \
  -p 443:443 \
  -p 9200:9200 \
  -e SSL=true \
  -v /path/to/kibana/certs:/etc/nginx/ssl:ro \  
  blacktop/elastic-stack
```

> **NOTE:** It will also set a basic auth **user/pass** of `admin/admin` if none are supplied.  
