#!/bin/bash

while true; do
    METRIC_PURGE_DELAY=${ES_METRIC_PURGE_DELAY:=1}
    FILE_PURGE_DELAY=${ES_FILE_PURGE_DELAY:=100}
    URL=${ES_URL:="localhost:9200"}
    #authorization="$ELSK_USER:$ELSK_PASS"
    echo "Checking purge for host '$URL'"

    # Get all indices
#    indices=$(curl --user $authorization "$URL/_cat/indices?h=i,id,creation.date.string")
    indices=$(curl  "$URL/_cat/indices?h=i,id,creation.date.string")
    currentdate=$(date +%s)
    metricdelay=$(($METRIC_PURGE_DELAY*86400))
    filedelay=$(($FILE_PURGE_DELAY*86400))

    # For every indices
    while read -r line; do
        # Only if we have metricbeat indice
        if [[ $line == metricbeat* ]]; then
            name="$(echo $line | cut -d' ' -f1)"
            date="$(echo $line | cut -d' ' -f3 | sed -e s/T.*//)"

            indicedate=$(date -d $date +%s)
            limitdate=$(($currentdate-$metricdelay))
            if [ "$limitdate" -ge "$indicedate" ]; then
                echo "Indice $name($date) has exceeded the retention delay of $METRIC_PURGE_DELAY, launching purge"
#                curl --user $authorization -XDELETE "$URL/$name"
                curl -XDELETE "$URL/$name"
            else
                echo "Indice $name($date) has not exceeded the retention delay of $METRIC_PURGE_DELAY, ($limitdate < $indicedate)"
            fi
        fi
        # Only if we have filebeat indice
        if [[ $line == filebeat* ]]; then
            name="$(echo $line | cut -d' ' -f1)"
            date="$(echo $line | cut -d' ' -f3 | sed -e s/T.*//)"

            indicedate=$(date -d $date +%s)
            limitdate=$(($currentdate-$filedelay))
            if [ "$limitdate" -ge "$indicedate" ]; then
                echo "Indice $name($date) has exceeded the retention delay of $PFILE_PURGE_DELAY, launching purge"
#                curl --user $authorization -XDELETE "$URL/$name"
                curl -XDELETE "$URL/$name"
            else
                echo "Indice $name($date) has not exceeded the retention delay of $FILE_PURGE_DELAY, ($limitdate < $indicedate)"
            fi
        fi
    done <<< "$indices"
    sleep 1h
done


